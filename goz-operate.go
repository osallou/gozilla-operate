package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/goware/emailx"
	"github.com/rs/cors"

	mongo "go.mongodb.org/mongo-driver/mongo"
	mongoOptions "go.mongodb.org/mongo-driver/mongo/options"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	gozilla "gitlab.inria.fr/osallou/gozilla-lib"

	elasticsearch "github.com/elastic/go-elasticsearch"
)

// Version of server
var Version string

const currentAPIversion = "v1.0"

type favContextKey string

// HomeHandler manages base entrypoint
var HomeHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	resp := map[string]interface{}{
		"version":    Version,
		"message":    "ok",
		"storage":    goz.Config.Storage.Type,
		"apiVersion": currentAPIversion,
		"cas":        goz.Config.Auth.CAS,
		"url":        goz.Config.URL,
	}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

// NYIHandler fake handler for not yet implemented handlers
var NYIHandler = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotImplemented)
	resp := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(resp)
}

// check validates authorization token/header
// if url query parameter user is set, switch authenticated user to requested user.
// Also support request header X-GOZ-AS-USER
// Authenticated user needs operator or admin rights
// Example:
//     curl -X PATCH https://goz.org/api/v1.0/operate/subject?user=myfriend
// Will operate subject modification as if was logged as user *myfriend*
// without user restrictions though

func check(r *http.Request) (gozilla.User, error) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr == nil && !claims.IsOperator() {
		claimsErr = fmt.Errorf("not allowed, need operator rights")
	}

	// If query param user, act as requested user
	actAsHeader := r.Header.Get("X-GOZ-AS-USER")
	keys, ok := r.URL.Query()["user"]
	if ok && len(keys[0]) == 1 && keys[0] != "" {
		actAsHeader = keys[0]
	}

	if actAsHeader != "" {
		actAsUser := gozilla.User{
			ID: keys[0],
		}
		actAs, actAsErr := actAsUser.Exists(goz)
		if actAsErr != nil {
			log.Error().Str("operator", claims.ID).Str("user", actAsUser.ID).Msg("user not found, cannot substitute")
			return claims, actAsErr
		}
		log.Debug().Str("operator", claims.ID).Str("user", actAs.ID).Msg("operator substitution operation")
		return actAs, nil
	}
	return claims, claimsErr
}

// UserListHandler TODO
var UserListHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("list users")
	NYIHandler(w, r)
}

// UserCreateHandler TODO
var UserCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("create users")
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	defer r.Body.Close()
	userInput := &gozilla.User{}
	err := json.NewDecoder(r.Body).Decode(userInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, userErr := userInput.Exists(goz)

	if userErr == nil {
		// User exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "user already exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	var emailErr error
	if userInput.Email != "" {
		emailErr = emailx.Validate(userInput.Email)
	} else {
		emailErr = fmt.Errorf("no email provided")
	}

	if emailErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": emailErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if userInput.Customer == "" {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "no customer provided"}
		json.NewEncoder(w).Encode(respError)
	} else {
		cust := gozilla.Customer{
			ID: userInput.Customer,
		}
		_, custErr := cust.Exists(goz)
		if custErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusForbidden)
			respError := map[string]interface{}{"message": "customer does not exist"}
			json.NewEncoder(w).Encode(respError)
		}
	}

	userInput.Register(goz)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	version := vars["apiversion"]
	remote := []string{goz.Config.URL, "api", version, "operate", "user", userInput.ID}
	w.Header().Add("Location", strings.Join(remote, "/"))

	resp := map[string]interface{}{"user": userInput}
	json.NewEncoder(w).Encode(resp)
}

// UserGetHandler TODO
var UserGetHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("get users")
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)
	userSearch := gozilla.User{
		ID: vars["user"],
	}
	user, userErr := userSearch.Exists(goz)
	// Do not show password
	user.Password = "****"
	if userErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "user does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"user": user}
	json.NewEncoder(w).Encode(resp)
}

// UserStatusDeactivateHandler TODO
var UserStatusDeactivateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("update users")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	userSearch := gozilla.User{
		ID: vars["user"],
	}

	user, userErr := userSearch.Exists(goz)

	if userErr != nil {
		// User does not exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "user does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	user.Status = gozilla.UserInactive

	user.Save(goz)
	user.Password = "****"

	w.Header().Add("Content-Type", "application/json")

	resp := map[string]interface{}{"user": user}
	json.NewEncoder(w).Encode(resp)
}

// UserStatusActivateHandler TODO
var UserStatusActivateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("update users")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	userSearch := gozilla.User{
		ID: vars["user"],
	}

	user, userErr := userSearch.Exists(goz)

	if userErr != nil {
		// User does not exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "user does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	user.Status = gozilla.UserActive

	user.Save(goz)
	user.Password = "****"

	w.Header().Add("Content-Type", "application/json")

	resp := map[string]interface{}{"user": user}
	json.NewEncoder(w).Encode(resp)
}

// UserUpdateHandler TODO
var UserUpdateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("update users")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	defer r.Body.Close()
	userInput := &gozilla.User{}
	err := json.NewDecoder(r.Body).Decode(userInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if userInput.ID != vars["user"] {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "user id and endpoint id are different"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	user, userErr := userInput.Exists(goz)

	if userErr != nil {
		// User does not exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "user does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	// Keep password, cannot be modified this way
	userInput.Password = user.Password

	var emailErr error
	if userInput.Email != "" {
		emailErr = emailx.Validate(userInput.Email)
	} else {
		emailErr = fmt.Errorf("no email provided")
	}

	if emailErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": emailErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if userInput.Customer == "" {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "no customer provided"}
		json.NewEncoder(w).Encode(respError)
	} else {
		cust := gozilla.Customer{
			ID: userInput.Customer,
		}
		_, custErr := cust.Exists(goz)
		if custErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusForbidden)
			respError := map[string]interface{}{"message": "customer does not exist"}
			json.NewEncoder(w).Encode(respError)
		}
	}

	userInput.Save(goz)
	userInput.Password = "****"

	w.Header().Add("Content-Type", "application/json")

	resp := map[string]interface{}{"user": userInput}
	json.NewEncoder(w).Encode(resp)
}

// UserDeleteHandler TODO
var UserDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("delete users")

	NYIHandler(w, r)
}

/* Customer handling */

// CustomerListHandler TODO
var CustomerListHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("list customers")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	customer := gozilla.Customer{}
	customers, cErr := customer.List(goz)
	if cErr != nil {
		log.Error().Err(cErr).Msg("customer list error")
	}
	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"customers": customers}
	json.NewEncoder(w).Encode(resp)
}

// CustomerCreateHandler TODO
var CustomerCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("create customers")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	defer r.Body.Close()
	custInput := &gozilla.Customer{}
	err := json.NewDecoder(r.Body).Decode(custInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, custErr := custInput.Exists(goz)

	if custErr == nil {
		// Customer exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "customer already exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	var emailErr error
	if custInput.Email != "" {
		emailErr = emailx.Validate(custInput.Email)
	} else {
		emailErr = fmt.Errorf("no email provided")
	}

	if emailErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": emailErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	planSearch := gozilla.PlanInfo{
		ID: custInput.Plan,
	}
	plan, planErr := planSearch.Exists(goz)
	if planErr != nil {
		log.Error().Err(planErr).Msg("customer creation failed")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "plan does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	custInput.History = make([]gozilla.PlanHistory, 1)
	now := time.Now()
	ts := now.Unix()
	custInput.History[0] = gozilla.PlanHistory{
		Plan: plan,
		From: ts,
		To:   0,
	}

	custInput.Create(goz)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	version := vars["apiversion"]
	remote := []string{goz.Config.URL, "api", version, "operate", "customer", custInput.ID}
	w.Header().Add("Location", strings.Join(remote, "/"))

	resp := map[string]interface{}{"customer": custInput}
	json.NewEncoder(w).Encode(resp)
}

// CustomerGetHandler TODO
// Query params:
//   users=1 returns list of customer users
//   subjects=1 return list of subjects owned by customer
var CustomerGetHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("get customers")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)
	customerSearch := gozilla.Customer{
		ID: vars["customer"],
	}
	customer, customerErr := customerSearch.Exists(goz)
	if customerErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "customer does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	users := make([]gozilla.User, 0)

	keys, ok := r.URL.Query()["users"]
	if ok && len(keys[0]) == 1 && keys[0] == "1" {
		users, _ = customer.Users(goz)
	}

	subjects := make([]gozilla.Subject, 0)

	keys, ok = r.URL.Query()["subjects"]
	if ok && len(keys[0]) == 1 && keys[0] == "1" {
		subjects, _ = customer.OwnedSubjects(goz)
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"customer": customer, "users": users, "subjects": subjects}
	json.NewEncoder(w).Encode(resp)
}

// CustomerUpdateHandler TODO
var CustomerUpdateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("update customers")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	defer r.Body.Close()
	custInput := &gozilla.Customer{}
	err := json.NewDecoder(r.Body).Decode(custInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if custInput.ID != vars["customer"] {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "customer id and endpoint id are different"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	customer, custErr := custInput.Exists(goz)

	if custErr != nil {
		// Customer does not exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "customer does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	var emailErr error
	if custInput.Email != "" {
		emailErr = emailx.Validate(custInput.Email)
	} else {
		emailErr = fmt.Errorf("no email provided")
	}

	if emailErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": emailErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if custInput.Plan == "" {
		custInput.Plan = customer.Plan
	}

	custInput.Save(goz)

	w.Header().Add("Content-Type", "application/json")

	resp := map[string]interface{}{"customer": custInput}
	json.NewEncoder(w).Encode(resp)
}

// CustomerDeleteHandler TODO
var CustomerDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("delete customers")

	NYIHandler(w, r)
}

/* plan handling */

// PlanListHandler TODO
var PlanListHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("list plans")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	plan := gozilla.PlanInfo{}
	plans, cErr := plan.List(goz)
	if cErr != nil {
		log.Error().Err(cErr).Msg("plan list error")
	}
	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"plans": plans}
	json.NewEncoder(w).Encode(resp)
}

// PlanCreateHandler TODO
var PlanCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("create plans")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	defer r.Body.Close()
	planInput := &gozilla.PlanInfo{}
	err := json.NewDecoder(r.Body).Decode(planInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, planErr := planInput.Exists(goz)

	if planErr == nil {
		// plan exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "plan already exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	planInput.Create(goz)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	version := vars["apiversion"]
	remote := []string{goz.Config.URL, "api", version, "operate", "plan", planInput.ID}
	w.Header().Add("Location", strings.Join(remote, "/"))

	resp := map[string]interface{}{"plan": planInput}
	json.NewEncoder(w).Encode(resp)
}

// PlanGetHandler TODO
// Query params:
// customers=1 shows customers linked with this plan
var PlanGetHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("get plans")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)
	planSearch := gozilla.PlanInfo{
		ID: vars["plan"],
	}
	plan, planErr := planSearch.Exists(goz)
	if planErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "plan does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	var customersID []string

	keys, ok := r.URL.Query()["customers"]
	if ok && len(keys[0]) == 1 && keys[0] == "1" {
		customers, _ := plan.UsedBy(goz)

		customersID = make([]string, len(customers))
		for index, customer := range customers {
			customersID[index] = customer.ID
		}
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"customers": customersID, "plan": plan}
	json.NewEncoder(w).Encode(resp)
}

// PlanUpdateHandler TODO
var PlanUpdateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("update plans")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	defer r.Body.Close()
	planInput := &gozilla.PlanInfo{}
	err := json.NewDecoder(r.Body).Decode(planInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if planInput.ID != vars["plan"] {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "plan id and endpoint id are different"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, planErr := planInput.Exists(goz)

	if planErr != nil {
		// plan does not exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "plan does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	planInput.Save(goz)

	w.Header().Add("Content-Type", "application/json")

	resp := map[string]interface{}{"plan": planInput}
	json.NewEncoder(w).Encode(resp)
}

// PlanDeleteHandler TODO
var PlanDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("delete plans")

	NYIHandler(w, r)
}

/* subject handling */

// SubjectListHandler TODO
var SubjectListHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)

	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("list subjects")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	subject := gozilla.Subject{}
	subjects, rErr := subject.List(goz)
	if rErr != nil {
		log.Error().Err(rErr).Msg("subject list error")
	}
	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"subjects": subjects}
	json.NewEncoder(w).Encode(resp)
}

// SubjectCreateHandler TODO
var SubjectCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("create subjects")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	defer r.Body.Close()
	subjectInput := &gozilla.Subject{}
	err := json.NewDecoder(r.Body).Decode(subjectInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, sErr := subjectInput.Exists(goz)

	if sErr == nil {
		// subject exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "subject already exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if subjectInput.Owner == "" {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "missing owner"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if subjectInput.Type == gozilla.SubjectOrg && subjectInput.Customer != "" {
		customer := gozilla.Customer{
			ID: subjectInput.Customer,
		}
		_, cErr := customer.Exists(goz)
		if cErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusForbidden)
			respError := map[string]interface{}{"message": "customer does not exists"}
			json.NewEncoder(w).Encode(respError)
			return
		}
	} else {
		log.Info().Str("subject", subjectInput.ID).Str("customer", claims.Customer).Msg("no customer provided, using user default")
		subjectInput.Customer = claims.Customer
	}

	subjectInput.Create(goz)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	version := vars["apiversion"]
	remote := []string{goz.Config.URL, "api", version, "operate", "subject", subjectInput.ID}
	w.Header().Add("Location", strings.Join(remote, "/"))

	resp := map[string]interface{}{"repo": subjectInput}
	json.NewEncoder(w).Encode(resp)
}

// SubjectGetHandler TODO
var SubjectGetHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	log.Debug().Str("user", claims.ID).Msg("get subjects")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)
	subjectSearch := gozilla.Subject{
		ID: vars["subject"],
	}
	subject, subjectErr := subjectSearch.Exists(goz)
	if subjectErr != nil {
		log.Error().Err(subjectErr).Msgf("subject not found %s", subjectSearch.ID)
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "subject does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	quota, quotaErr := goz.Storage.GetQuota(goz, subject.ID)
	if quotaErr != nil {
		log.Warn().Str("subject", subject.ID).Msg("failed to get quota")
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"subject": subject, "quota": quota}
	json.NewEncoder(w).Encode(resp)
}

// SubjectUpdateHandler TODO
var SubjectUpdateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	log.Debug().Str("user", claims.ID).Msg("update subjects")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	defer r.Body.Close()
	subjectInput := &gozilla.Subject{}
	err := json.NewDecoder(r.Body).Decode(subjectInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if subjectInput.ID != vars["subject"] {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "subject id and endpoint id are different"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	subject, sErr := subjectInput.Exists(goz)

	if sErr != nil {
		// subject does not exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "subject does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if subjectInput.Customer != "" {
		customer := gozilla.Customer{
			ID: subjectInput.Customer,
		}
		_, cErr := customer.Exists(goz)
		if cErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusForbidden)
			respError := map[string]interface{}{"message": "customer does not exists"}
			json.NewEncoder(w).Encode(respError)
			return
		}
	} else {
		log.Info().Str("subject", subjectInput.ID).Str("customer", claims.Customer).Msg("no customer provided, using user default")
		subjectInput.Customer = claims.Customer
	}

	if subjectInput.Owner == "" {
		subjectInput.Owner = claims.ID
	}

	subjectInput.Save(goz)

	if subject.Customer != subjectInput.Customer {
		if subject.Customer != "" {
			gozilla.CustomerStatSubjects(goz, subject.Customer)
		}
		if subjectInput.Customer != "" {
			gozilla.CustomerStatSubjects(goz, subjectInput.Customer)
		}
	}

	w.Header().Add("Content-Type", "application/json")

	resp := map[string]interface{}{"subject": subjectInput}
	json.NewEncoder(w).Encode(resp)
}

// SubjectDeleteHandler TODO
var SubjectDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	log.Debug().Str("user", claims.ID).Msg("delete subjects")

	NYIHandler(w, r)
}

/* repo handling */

// RepoListHandler TODO
var RepoListHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("list repos")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	repo := gozilla.Repo{
		Subject: vars["subject"],
	}
	repos, rErr := repo.List(goz)
	if rErr != nil {
		log.Error().Err(rErr).Msg("repo list error")
	}
	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"repos": repos}
	json.NewEncoder(w).Encode(resp)
}

// RepoCreateHandler TODO
var RepoCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("create repos")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	defer r.Body.Close()
	repoInput := &gozilla.Repo{}
	err := json.NewDecoder(r.Body).Decode(repoInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	repoInput.Subject = vars["subject"]
	if repoInput.Owner == "" {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "owner empty"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	subjectSearch := gozilla.Subject{
		ID: vars["subject"],
	}
	_, subjectErr := subjectSearch.Exists(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "subject does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, repoErr := repoInput.Exists(goz)

	if repoErr == nil {
		// repo exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "repository already exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	repoInput.Create(goz)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	version := vars["apiversion"]
	remote := []string{goz.Config.URL, "api", version, "operate", "repo", repoInput.ID}
	w.Header().Add("Location", strings.Join(remote, "/"))

	resp := map[string]interface{}{"repo": repoInput}
	json.NewEncoder(w).Encode(resp)
}

// RepoGetHandler TODO
var RepoGetHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("get repos")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)
	repoSearch := gozilla.Repo{
		Subject: vars["subject"],
		ID:      vars["repo"],
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"repo": repo}
	json.NewEncoder(w).Encode(resp)
}

// RepoUpdateHandler TODO
var RepoUpdateHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("update repos")

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	goz.User = claims

	vars := mux.Vars(r)

	defer r.Body.Close()
	repoInput := &gozilla.Repo{}
	err := json.NewDecoder(r.Body).Decode(repoInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if repoInput.ID != vars["repo"] {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "repo id and endpoint id are different"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, repoErr := repoInput.Exists(goz)

	if repoErr != nil {
		// repo does not exists
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repository does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	repoInput.Save(goz)

	w.Header().Add("Content-Type", "application/json")

	resp := map[string]interface{}{"repo": repoInput}
	json.NewEncoder(w).Encode(resp)
}

// RepoDeleteHandler TODO
var RepoDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	claims, claimsErr := check(r)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	log.Debug().Str("user", claims.ID).Msg("delete repos")

	NYIHandler(w, r)
}

func setRoutes(r *mux.Router) {
	r.HandleFunc("/api", HomeHandler).Methods("GET")

	r.HandleFunc("/api/{apiversion}/operate/user", UserListHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/operate/user", UserCreateHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/operate/user/{user}", UserGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/operate/user/{user}", UserUpdateHandler).Methods("PATCH")
	r.HandleFunc("/api/{apiversion}/operate/user/{user}", UserDeleteHandler).Methods("DELETE")
	r.HandleFunc("/api/{apiversion}/operate/user/{user}/status", UserStatusActivateHandler).Methods("PUT")
	r.HandleFunc("/api/{apiversion}/operate/user/{user}/status", UserStatusDeactivateHandler).Methods("DELETE")

	r.HandleFunc("/api/{apiversion}/operate/customer", CustomerListHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/operate/customer", CustomerCreateHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/operate/customer/{customer}", CustomerGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/operate/customer/{customer}", CustomerUpdateHandler).Methods("PATCH")
	r.HandleFunc("/api/{apiversion}/operate/customer/{customer}", CustomerDeleteHandler).Methods("DELETE")

	r.HandleFunc("/api/{apiversion}/operate/plan", PlanListHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/operate/plan", PlanCreateHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/operate/plan/{plan}", PlanGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/operate/plan/{plan}", PlanUpdateHandler).Methods("PATCH")
	r.HandleFunc("/api/{apiversion}/operate/plan/{plan}", PlanDeleteHandler).Methods("DELETE")

	r.HandleFunc("/api/{apiversion}/operate/subject", SubjectListHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/operate/subject", SubjectCreateHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/operate/subject/{subject}", SubjectGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/operate/subject/{subject}", SubjectUpdateHandler).Methods("PATCH")
	r.HandleFunc("/api/{apiversion}/operate/subject/{subject}", SubjectDeleteHandler).Methods("DELETE")

	r.HandleFunc("/api/{apiversion}/operate/repo/{subject}", RepoListHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/operate/repo/{subject}", RepoCreateHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/operate/repo/{subject}/{repo}", RepoGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/operate/repo/{subject}/{repo}", RepoUpdateHandler).Methods("PATCH")
	r.HandleFunc("/api/{apiversion}/operate/repo/{subject}/{repo}", RepoDeleteHandler).Methods("DELETE")
}

func gozMiddleware(gozCtx gozilla.GozContext, next http.Handler) http.Handler {
	goz := favContextKey("goz")
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), goz, gozCtx)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func main() {

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	config := gozilla.LoadConfig()

	mongoClient, err := mongo.NewClient(mongoOptions.Client().ApplyURI(config.Mongo.URL))
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}
	ctx, cancelMongo := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelMongo()

	err = mongoClient.Connect(ctx)
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}

	storage, storageErr := gozilla.NewStorageHandler(config)
	if storageErr != nil {
		log.Error().Err(storageErr).Msg("storage error")
		os.Exit(1)
	}

	amqpHandler, amqpErr := gozilla.NewAmqpHandler(config)
	if amqpErr != nil {
		log.Error().Err(amqpErr).Msg("rabbitmq error")
		os.Exit(1)
	}

	esCfg := elasticsearch.Config{
		Addresses: config.Elastic,
	}
	es, err := elasticsearch.NewClient(esCfg)
	if err != nil {
		log.Error().Err(err).Msgf("Error creating the client: %s", err)
		os.Exit(1)
	}

	gozCtx := gozilla.GozContext{
		Mongo:       mongoClient,
		Config:      config,
		Storage:     storage,
		AmqpHandler: amqpHandler,
		Elastic:     es,
	}

	consulErr := gozilla.ConsulDeclare(gozCtx, "gozoperator")
	if consulErr != nil {
		log.Error().Err(consulErr).Msgf("Failed to register: %s", consulErr.Error())
		os.Exit(1)
	}

	r := mux.NewRouter()
	setRoutes(r)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedHeaders:   []string{"Authorization", "Content-Type"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE"},
	})
	handler := c.Handler(r)

	loggedRouter := handlers.LoggingHandler(os.Stdout, handler)
	ctxHandler := gozMiddleware(gozCtx, loggedRouter)

	srv := &http.Server{
		Handler: ctxHandler,
		Addr:    fmt.Sprintf("%s:%d", config.Web.Listen, config.Web.Port),
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	srv.ListenAndServe()

}
