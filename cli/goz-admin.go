package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	"github.com/rs/zerolog"

	"github.com/goware/emailx"
	"github.com/olekukonko/tablewriter"

	"github.com/dustin/go-humanize"

	gozilla "gitlab.inria.fr/osallou/gozilla-lib"
)

// Version version of tool
var Version string

// OptionsDef command line options
type OptionsDef struct {
	UID    string
	APIKEY string
	URL    string
}

func promptConfirm(question string) bool {
	if os.Getenv("GOZ_NONINTERACTIVE") == "1" {
		return true
	}
	fmt.Print(question + "[y/n]:")
	var input string
	fmt.Scanln(&input)
	if input == "y" {
		return true
	}
	return false
}

func send(options OptionsDef, url string, method string, data []byte) ([]byte, error) {
	client := &http.Client{}
	var req *http.Request
	if method == "GET" {
		req, _ = http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s", options.URL, url), nil)
	} else {
		var buffer *bytes.Buffer
		if data != nil {
			buffer = bytes.NewBuffer(data)
		}
		req, _ = http.NewRequest(method, fmt.Sprintf("%s/api/v1.0/%s", options.URL, url), buffer)
	}
	if options.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", options.UID)
		req.Header.Set("X-GOZ-APIKEY", options.APIKEY)
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			return body, fmt.Errorf("Error: %s", body)
		}
		return body, fmt.Errorf("Show command failed")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return body, err
	}
	return body, nil
}

func cliUsage() {
	flag.PrintDefaults()
	fmt.Printf("Env:\n")
	fmt.Printf(" * GOZ_NONINTERACTIVE: set to 1 to skip interactive prompts")
	fmt.Printf("Subcommands:\n")
	fmt.Printf(" * user\n")
	fmt.Printf(" * customer\n")
	fmt.Printf(" * plan\n")
	fmt.Printf(" * subject\n")
}

func handleUser(gozCtx gozilla.GozContext, options OptionsDef, args []string) error {
	var err error

	switch args[0] {
	case "deactivate":
		userID := ""
		cmdOptions := flag.NewFlagSet("show options", flag.ExitOnError)
		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			userID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing user identifier, command: user deactivate XXX")
		}
		_, err := send(options, "operate/user/"+userID+"/status", "DELETE", nil)
		if err != nil {
			return err
		}
		print("user deactivated")
		break
	case "activate":
		userID := ""
		cmdOptions := flag.NewFlagSet("show options", flag.ExitOnError)
		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			userID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing user identifier, command: user activate XXX")
		}
		_, err := send(options, "operate/user/"+userID+"/status", "DELETE", nil)
		if err != nil {
			return err
		}
		print("user activated")
		break
	case "show":
		userID := ""
		cmdOptions := flag.NewFlagSet("show options", flag.ExitOnError)
		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			userID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing user identifier, command: user show XXX")
		}

		body, err := send(options, "operate/user/"+userID, "GET", nil)
		if err != nil {
			return err
		}

		type UserShowResp struct {
			User gozilla.User `json:"user"`
		}
		var resp UserShowResp
		err = json.Unmarshal(body, &resp)
		if err != nil {
			return err
		}
		user := resp.User

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Email", "Customer", "API Key", "Status"})

		status := "active"
		if user.Status == 1 {
			status = "inactive"
		}
		table.Append([]string{user.ID, user.Email, user.Customer, user.APIKey, status})
		table.Render()
		break
	case "create":
		userID := ""
		cmdOptions := flag.NewFlagSet("create options", flag.ExitOnError)
		userPassword := cmdOptions.String("password", "", "user password")
		userCustomer := cmdOptions.String("customer", "", "customer id")
		userProfile := cmdOptions.String("profile", "user", "user profile [admin|operator|user]")
		userEmail := cmdOptions.String("email", "", "user email")

		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			userID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing user identifier, command: user create XXX")
		}
		if *userPassword == "" {
			return fmt.Errorf("empty password")
		}

		user := gozilla.User{
			ID:       userID,
			Password: *userPassword,
			Customer: *userCustomer,
			Email:    *userEmail,
		}
		switch *userProfile {
		case "":
			user.Profile = gozilla.UserProfileUser
		case "admin":
			user.Profile = gozilla.UserProfileAdmin
		case "operator":
			user.Profile = gozilla.UserProfileOperator
		default:
			user.Profile = gozilla.UserProfileUser
		}

		data, _ := json.Marshal(user)
		body, err := send(options, "operate/user", "POST", data)
		if err != nil {
			return err
		}
		type UserCreateResp struct {
			User gozilla.User `json:"user"`
		}
		var createdUser UserCreateResp
		json.Unmarshal(body, &createdUser)
		fmt.Println("user created")

		user = createdUser.User
		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Email", "Customer", "API Key", "Status"})

		status := "active"
		if user.Status == 1 {
			status = "inactive"
		}
		table.Append([]string{user.ID, user.Email, user.Customer, user.APIKey, status})
		table.Render()
		break
	default:
		userUsage()
		return fmt.Errorf("command unknown")
	}
	return err
}

func handleCustomer(gozCtx gozilla.GozContext, options OptionsDef, args []string) error {
	var err error

	switch args[0] {
	case "show":
		customerID := ""
		cmdOptions := flag.NewFlagSet("show options", flag.ExitOnError)
		showUsers := cmdOptions.Bool("users", false, "show customer users")
		showSubjects := cmdOptions.Bool("subjects", false, "show customer owned subjects")

		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			customerID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing customer identifier, command: customer show XXX")
		}

		addUsers := 0
		addSubjects := 1
		if *showUsers {
			addUsers = 1
		}
		if *showSubjects {
			addSubjects = 1
		}
		body, err := send(options, fmt.Sprintf("operate/customer/%s?users=%d&subjects=%d", customerID, addUsers, addSubjects), "GET", nil)
		if err != nil {
			return err
		}

		type CustomerShowResp struct {
			Customer gozilla.Customer  `json:"customer"`
			Users    []gozilla.User    `json:"users"`
			Subjects []gozilla.Subject `json:"subjects"`
		}
		var resp CustomerShowResp
		err = json.Unmarshal(body, &resp)
		if err != nil {
			return err
		}
		customer := resp.Customer

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Name", "Email", "Plan"})
		table.Append([]string{customer.ID, customer.Name, customer.Email, customer.Plan})
		table.Render()

		if len(resp.Users) > 0 {
			table = tablewriter.NewWriter(os.Stdout)
			table.SetHeader([]string{"User ID"})
			users := resp.Users
			for _, user := range users {
				table.Append([]string{user.ID})
			}
			table.Render()
		}

		if len(resp.Subjects) > 0 {
			table = tablewriter.NewWriter(os.Stdout)
			table.SetHeader([]string{"Subject", "Owner"})
			subjects := resp.Subjects
			for _, subject := range subjects {
				table.Append([]string{subject.ID, subject.Owner})
			}
			table.Render()
		}

		break
	case "list":
		body, err := send(options, "operate/customer", "GET", nil)
		if err != nil {
			return err
		}

		type CustomerListResp struct {
			Customers []gozilla.Customer `json:"customers"`
		}
		var resp CustomerListResp
		err = json.Unmarshal(body, &resp)
		if err != nil {
			return err
		}
		customers := resp.Customers
		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Name", "Email", "Plan"})
		for _, customer := range customers {
			table.Append([]string{customer.ID, customer.Name, customer.Email, customer.Plan})
		}
		table.Render()
		break
	case "create":
		customerID := ""
		cmdOptions := flag.NewFlagSet("create options", flag.ExitOnError)
		custPlan := cmdOptions.String("plan", "free", "customer plan [free by default]")
		custName := cmdOptions.String("name", "", "customer name for correspondance")
		custAddress := cmdOptions.String("address", "", "customer address")
		custCity := cmdOptions.String("city", "", "customer city")
		custCountry := cmdOptions.String("country", "", "customer country")
		custEmail := cmdOptions.String("email", "", "customer email")
		custPhone := cmdOptions.String("phone", "", "customer phone number")
		custDesc := cmdOptions.String("description", "", "description")

		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			customerID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing customer identifier, command: customer create XXX")
		}

		if *custEmail == "" {
			return fmt.Errorf("empty email")
		}
		emailErr := emailx.Validate(*custEmail)
		if emailErr != nil {
			return fmt.Errorf("email is not valid")
		}

		customer := gozilla.Customer{
			ID:          customerID,
			Plan:        *custPlan,
			Name:        *custName,
			Address:     *custAddress,
			City:        *custCity,
			Country:     *custCountry,
			Email:       *custEmail,
			Phone:       *custPhone,
			Description: *custDesc,
		}

		data, _ := json.Marshal(customer)
		body, err := send(options, "operate/customer", "POST", data)
		if err != nil {
			return err
		}

		type CustomerCreateResp struct {
			Customer gozilla.Customer `json:"customer"`
		}
		var customerResp CustomerCreateResp

		json.Unmarshal(body, &customerResp)

		customer = customerResp.Customer

		fmt.Println("customer created")

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Name", "Email", "Plan"})
		table.Append([]string{customer.ID, customer.Name, customer.Email, customer.Plan})
		table.Render()
		break
	default:
		customerUsage()
		return fmt.Errorf("command unknown")
	}
	return err
}

func handlePlan(gozCtx gozilla.GozContext, options OptionsDef, args []string) error {
	var err error

	switch args[0] {
	case "show":
		planID := ""
		cmdOptions := flag.NewFlagSet("show options", flag.ExitOnError)
		planCustomers := cmdOptions.Bool("customers", false, "show customers using this plan")

		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			planID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing plan identifier, command: plan show XXX")
		}

		showCustomers := 0
		if *planCustomers {
			showCustomers = 1
		}
		body, err := send(options, fmt.Sprintf("operate/plan/"+planID+"?customers=%d", showCustomers), "GET", nil)
		if err != nil {
			return err
		}

		type PlanResp struct {
			Plan      gozilla.PlanInfo `json:"plan"`
			Customers []string         `json:"customers"`
		}
		var planResp PlanResp

		json.Unmarshal(body, &planResp)

		plan := planResp.Plan

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Kind", "Quota Size", "Quota Subjects"})
		nbsubject := strconv.Itoa(plan.Quota.Subjects)
		table.Append([]string{plan.ID, plan.Kind.String(), plan.Quota.Size, nbsubject})
		table.Render()

		if *planCustomers == true {
			customers := planResp.Customers
			table := tablewriter.NewWriter(os.Stdout)
			table.SetHeader([]string{"Customers ID"})
			for _, customer := range customers {
				table.Append([]string{customer})
			}
			table.Render()
		}
		break
	case "list":

		body, err := send(options, "operate/plan", "GET", nil)
		if err != nil {
			return err
		}
		type PlanListResp struct {
			Plans []gozilla.PlanInfo `json:"plans"`
		}
		var planResp PlanListResp
		json.Unmarshal(body, &planResp)
		plans := planResp.Plans

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Kind", "Quota Size", "Quota Subjects"})
		for _, plan := range plans {
			nbsubject := strconv.Itoa(plan.Quota.Subjects)
			table.Append([]string{plan.ID, plan.Kind.String(), plan.Quota.Size, nbsubject})
		}
		table.Render()
		break
	case "create":
		planID := ""
		cmdOptions := flag.NewFlagSet("create options", flag.ExitOnError)
		planDesc := cmdOptions.String("description", "", "description")
		planMaxQuotaSubjects := cmdOptions.Int("subjects", 10, "max number of subjects")
		planMaxQuotaSize := cmdOptions.String("size", "1G", "max size of each repo [default 1G]")

		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			planID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing plan identifier, command: plan create XXX")
		}

		plan := gozilla.PlanInfo{
			ID: planID,
			Quota: gozilla.Quota{
				Subjects: *planMaxQuotaSubjects,
				Size:     *planMaxQuotaSize,
			},
			Kind:        gozilla.PlanCustomer,
			Description: *planDesc,
		}

		data, _ := json.Marshal(plan)
		body, err := send(options, "operate/plan", "POST", data)
		if err != nil {
			return err
		}
		type PlanListResp struct {
			Plan gozilla.PlanInfo `json:"plan"`
		}
		var planResp PlanListResp
		json.Unmarshal(body, &planResp)
		plan = planResp.Plan

		fmt.Println("plan created")
		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Kind", "Quota Size", "Quota Subjects"})
		nbsubject := strconv.Itoa(plan.Quota.Subjects)
		table.Append([]string{plan.ID, plan.Kind.String(), plan.Quota.Size, nbsubject})

		table.Render()
		break
	default:
		planUsage()
		return fmt.Errorf("command unknown")
	}
	return err
}

func getSubjectType(t gozilla.SubjectType) string {
	switch t {
	case gozilla.SubjectOrg:
		return "organization"
	case gozilla.SubjectPersonal:
		return "personal"
	default:
		return "unknown"
	}
}

func handleSubject(gozCtx gozilla.GozContext, options OptionsDef, args []string) error {
	var err error

	switch args[0] {
	case "show":
		subjectID := ""
		cmdOptions := flag.NewFlagSet("show options", flag.ExitOnError)

		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			subjectID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing subject identifier, command: subject show XXX")
		}

		body, err := send(options, "operate/subject/"+subjectID, "GET", nil)
		if err != nil {
			return err
		}

		type SubjectResp struct {
			Subject gozilla.Subject `json:"subject"`
			Quota   int64           `json:"quota"`
		}
		var subjectResp SubjectResp

		json.Unmarshal(body, &subjectResp)

		subject := subjectResp.Subject

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Owner", "Customer", "Quota usage"})
		//quota := strconv.FormatInt(subjectResp.Quota, 10)
		quota := humanize.Bytes(uint64(subjectResp.Quota))
		table.Append([]string{subject.ID, subject.Owner, subject.Customer, quota})
		table.Render()

		fmt.Println("** Members **")
		table = tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Role"})
		for _, member := range subject.Members {
			table.Append([]string{member.User, strconv.Itoa(int(member.Profile))})
		}
		table.Render()

		break
	case "list":
		cmdOptions := flag.NewFlagSet("list options", flag.ExitOnError)
		var userID string
		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			userID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing user identifier, command: subject list XXX")
		}

		body, err := send(options, "operate/subject?user="+userID, "GET", nil)
		if err != nil {
			return err
		}
		type SubjectListResp struct {
			Subjects []gozilla.Subject `json:"subjects"`
		}
		var subjectResp SubjectListResp
		json.Unmarshal(body, &subjectResp)
		subjects := subjectResp.Subjects

		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Owner", "Customer", "Type"})
		for _, subject := range subjects {
			table.Append([]string{subject.ID, subject.Owner, subject.Customer, getSubjectType(subject.Type)})
		}
		table.Render()
		break
	case "create":
		subjectID := ""
		cmdOptions := flag.NewFlagSet("create options", flag.ExitOnError)
		subjectDesc := cmdOptions.String("description", "", "description")
		subjectOwner := cmdOptions.String("owner", "", "owner user id")
		subjectCustomer := cmdOptions.String("customer", "", "customer id (empty if none for personal subject")
		subjectVisibility := cmdOptions.String("visibility", "public", "visibility [public*|protected|private]")
		subjectType := cmdOptions.String("type", "org", "kind of subject [perso|org*]")

		cmdOptions.Parse(args[1:])
		remainingArgs := cmdOptions.Args()
		if len(remainingArgs) == 1 {
			subjectID = remainingArgs[0]
		} else {
			return fmt.Errorf("missing subject identifier, command: subject create XXX")
		}

		if *subjectCustomer == "" && *subjectType == "org" {
			return fmt.Errorf("missing customer for organisational subject")
		}

		if *subjectOwner == "" {
			return fmt.Errorf("missing owner")
		}

		var sType gozilla.SubjectType
		var sVisibility gozilla.VisibilityType

		switch *subjectVisibility {
		case "public":
			sVisibility = gozilla.VisibilityPublic
			break
		case "protected":
			sVisibility = gozilla.VisibilityProtected
			break
		case "private":
			sVisibility = gozilla.VisibilityPrivate
			break
		default:
			return fmt.Errorf("unknown visisbility value")
		}

		switch *subjectType {
		case "perso":
			sType = gozilla.SubjectPersonal
			break
		case "org":
			sType = gozilla.SubjectOrg
		default:
			return fmt.Errorf("unknown subject type")
		}

		subject := gozilla.Subject{
			ID:          subjectID,
			Type:        sType,
			Owner:       *subjectOwner,
			Customer:    *subjectCustomer,
			Description: *subjectDesc,
			Members:     make([]gozilla.Member, 0),
			Visibility:  sVisibility,
		}

		data, _ := json.Marshal(subject)
		body, err := send(options, "operate/subject", "POST", data)
		if err != nil {
			return err
		}
		type SubjectListResp struct {
			Subject gozilla.Subject `json:"subject"`
		}
		var subjectResp SubjectListResp
		json.Unmarshal(body, &subjectResp)
		subject = subjectResp.Subject

		fmt.Println("subject created")
		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Owner", "Customer"})
		table.Append([]string{subject.ID, subject.Owner, subject.Customer})

		table.Render()
		break
	default:
		subjectUsage()
		return fmt.Errorf("command unknown")
	}
	return err
}

func userUsage() {
	fmt.Println("User commands:")
	fmt.Println(" * show ID: show user ID in details")
	fmt.Println(" * create ID: creates a new user")
	fmt.Println(" * delete ID: removes user")
	fmt.Println(" * activate ID: update user ID status to active")
	fmt.Println(" * deactivate ID: update user ID status to inactive")

}

func customerUsage() {
	fmt.Println("Customer commands:")
	fmt.Println(" * list: list customers")
	fmt.Println(" * show ID: show customer ID in details")
	fmt.Println(" * create ID: creates a new customer")
	fmt.Println(" * delete ID: removes customer")
}

func planUsage() {
	fmt.Println("Plan commands:")
	fmt.Println(" * list: list plans")
	fmt.Println(" * show ID: show plan ID in details")
	fmt.Println(" * create ID: creates a new plan")
	fmt.Println(" * delete ID: removes plan")
}

func subjectUsage() {
	fmt.Println("Subject commands:")
	fmt.Println(" * list: list user subject")
	fmt.Println(" * show ID: show subject ID in details")
	fmt.Println(" * create ID: creates a new subject")
	fmt.Println(" * delete ID: removes subject")
}

func main() {

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if os.Getenv("GOZ_DEBUG") == "1" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	gozCtx := gozilla.GozContext{}

	options := OptionsDef{
		UID:    os.Getenv("GOZ_USER"),
		APIKEY: os.Getenv("GOZ_APIKEY"),
		URL:    os.Getenv("GOZ_URL"),
	}

	var userID string
	var apiKey string
	var url string
	var showVersion bool

	flag.StringVar(&userID, "user", "", "login identifier")
	flag.StringVar(&apiKey, "apikey", "", "Authentication API Key")
	flag.StringVar(&url, "url", "", "URL to Gozilla host")
	flag.BoolVar(&showVersion, "version", false, "show client version")
	flag.Usage = cliUsage
	flag.Parse()

	if showVersion {
		fmt.Printf("Version: %s\n", Version)
		os.Exit(0)
	}

	if apiKey != "" {
		options.APIKEY = apiKey
	}
	if url != "" {
		options.URL = url
	}
	if userID != "" {
		options.UID = userID
	}

	if options.UID == "" || options.URL == "" || options.APIKEY == "" {
		fmt.Println("user, apikey and url options must not be empty")
		os.Exit(1)
	}

	args := flag.Args()

	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	var cmdErr error

	switch args[0] {
	case "user":
		if len(args) == 1 {
			userUsage()
			os.Exit(1)
		}
		cmdErr = handleUser(gozCtx, options, args[1:])
		break
	case "customer":
		if len(args) == 1 {
			customerUsage()
			os.Exit(1)
		}
		cmdErr = handleCustomer(gozCtx, options, args[1:])
		break
	case "plan":
		if len(args) == 1 {
			planUsage()
			os.Exit(1)
		}
		cmdErr = handlePlan(gozCtx, options, args[1:])
		break
	case "subject":
		if len(args) == 1 {
			subjectUsage()
			os.Exit(1)
		}
		cmdErr = handleSubject(gozCtx, options, args[1:])
		break
	default:
		cmdErr = fmt.Errorf("unknown command: %s", os.Args[1])
	}

	if cmdErr != nil {
		fmt.Printf("%s\n", cmdErr)
		os.Exit(1)
	}

}
