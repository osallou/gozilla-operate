module gozadmin

go 1.13

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/goware/emailx v0.2.0
	github.com/olekukonko/tablewriter v0.0.4
	github.com/rs/zerolog v1.18.0
	gitlab.inria.fr/osallou/gozilla-lib v0.0.0-20200318133931-4d4470927f31
	go.mongodb.org/mongo-driver v1.3.1
)
